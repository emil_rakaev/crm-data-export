from urllib import request
import json
import csv
from io import StringIO


headers = {
    'x-parse-username': 'emil',
    'content-type': 'application/json'
}

req = request.Request('http://0.0.0.0:8080/api/v2/active_cases/', method='POST', headers=headers)

data = {
    "ampStatus": ["Hold"],
    "limit": 50000,
}

data = json.dumps(data).encode()

r = request.urlopen(req, data=data)
json_obj = json.load(r)

titles = ['isOpened', 'firstTrigger', 'lastTrigger', 'aircraftId', 'system', 'tags', 'type', 'faultType', 'triggers',
          'activeDays', 'activeFlights', 'events', 'workOrder', 'referenceDate', 'lastComment']

writer_file = StringIO()
writer = csv.writer(writer_file, dialect='excel')


writer.writerow(titles)
for i in json_obj:
    writer.writerow(
        [i['isOpened'], i['firstTrigger'], i['lastTrigger'], i['aircraftId'], i['system'],
         i['tags'], i['type'], i['faultType'], i['triggers'], i['activeDays'],
         i['activeFlights'], i['events'], ['workOrder'], i['referenceDate'], i['lastComment']]
    )